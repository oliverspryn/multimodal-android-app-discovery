package com.oliverspryn.android.multimodal.ui.layoutmodes

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.unit.dp
import com.oliverspryn.android.multimodal.MainActivity
import com.oliverspryn.android.multimodal.utils.screen.Dimension
import com.oliverspryn.android.multimodal.utils.screen.ScreenClassifier
import com.oliverspryn.android.multimodal.utils.screen.WindowSizeClass
import io.mockk.spyk
import io.mockk.verify
import org.junit.Rule
import org.junit.Test

@ExperimentalMaterial3Api
class LayoutModesRouteTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    private val viewModel = spyk(LayoutModesViewModel()) // spyk() for seeing if functions are called on the model

    @Test
    fun whenTheScreenIsFullyOpenedAndAnArticleIsNotSelectedItShowsTheLayoutModesListScreen() {
        val classifier = ScreenClassifier.FullyOpened(
            height = Dimension(
                dp = 1920.dp,
                sizeClass = WindowSizeClass.Compact
            ),
            width = Dimension(
                dp = 1080.dp,
                sizeClass = WindowSizeClass.Compact
            )
        )

        viewModel.closeArticle()

        composeTestRule.setContent {
            LayoutModesRoute(
                layoutModesViewModel = viewModel,
                screenClassifier = classifier
            )
        }

        composeTestRule
            .onNodeWithTag("LayoutModesListScreen")
            .assertExists()
            .assertIsDisplayed()
    }

    @Test
    fun whenTheScreenIsFullyOpenedAndAnArticleIsSelectedItShowsTheLayoutModesDetailsScreen() {
        val classifier = ScreenClassifier.FullyOpened(
            height = Dimension(
                dp = 1920.dp,
                sizeClass = WindowSizeClass.Compact
            ),
            width = Dimension(
                dp = 1080.dp,
                sizeClass = WindowSizeClass.Compact
            )
        )

        viewModel.openArticle()

        composeTestRule.setContent {
            LayoutModesRoute(
                layoutModesViewModel = viewModel,
                screenClassifier = classifier
            )
        }

        composeTestRule
            .onNodeWithTag("LayoutModesDetailsScreen")
            .assertExists()
            .assertIsDisplayed()
    }

    @Test
    fun whenTheBackButtonIsPressedOnTheDetailsScreenItClosesTheArticle() {
        val classifier = ScreenClassifier.FullyOpened(
            height = Dimension(
                dp = 1920.dp,
                sizeClass = WindowSizeClass.Compact
            ),
            width = Dimension(
                dp = 1080.dp,
                sizeClass = WindowSizeClass.Compact
            )
        )

        viewModel.openArticle()

        composeTestRule.setContent {
            LayoutModesRoute(
                layoutModesViewModel = viewModel,
                screenClassifier = classifier
            )
        }

        composeTestRule
            .activity
            .onBackPressed()

        verify {
            viewModel.closeArticle()
        }
    }
}