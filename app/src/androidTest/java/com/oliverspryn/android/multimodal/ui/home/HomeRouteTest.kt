package com.oliverspryn.android.multimodal.ui.home

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import io.mockk.mockk
import io.mockk.verify
import org.junit.Rule
import org.junit.Test

class HomeRouteTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun itShowsTheHomeScreen() {
        composeTestRule.setContent {
            HomeRoute(
                onDeviceModeButtonTap = { },
                onLayoutModeButtonTap = { }
            )
        }

        composeTestRule
            .onNodeWithTag("HomeScreen")
            .assertExists()
            .assertIsDisplayed()
    }

    @Test
    fun itInvokesOnDeviceModeButtonTapWhenTheDeviceModesButtonIsTapped() {
        val onDeviceModeButtonTap: () -> Unit = mockk()

        composeTestRule.setContent {
            HomeRoute(
                onDeviceModeButtonTap = onDeviceModeButtonTap,
                onLayoutModeButtonTap = { }
            )
        }

        composeTestRule
            .onNodeWithTag("ViewDeviceModesButton")
            .performClick()

        verify {
            onDeviceModeButtonTap.invoke()
        }
    }
}