package com.oliverspryn.android.multimodal.ui.devicemodes

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.unit.dp
import com.oliverspryn.android.multimodal.utils.screen.Dimension
import com.oliverspryn.android.multimodal.utils.screen.ScreenClassifier
import com.oliverspryn.android.multimodal.utils.screen.WindowSizeClass
import org.junit.Rule
import org.junit.Test

class DeviceModesRouteTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    private val classifier = ScreenClassifier.FullyOpened(
        height = Dimension(
            dp = 1920.dp,
            sizeClass = WindowSizeClass.Expanded
        ),
        width = Dimension(
            dp = 1080.dp,
            sizeClass = WindowSizeClass.Expanded
        )
    )

    @Test
    fun itShowsTheDeviceModesScreen() {
        composeTestRule.setContent {
            DeviceModesRoute(
                screenClassifier = classifier
            )
        }

        composeTestRule
            .onNodeWithTag("DeviceModesScreen")
            .assertExists()
            .assertIsDisplayed()
    }

    @Test
    fun itShowsTheClassNameOfTheScreenClassifierAsTheTitle() {
        composeTestRule.setContent {
            DeviceModesRoute(
                screenClassifier = classifier
            )
        }

        composeTestRule
            .onNodeWithTag("ScreenClassifierTitle")
            .assertTextEquals("FullyOpened")
            .assertIsDisplayed()
    }

    @Test
    fun itShowsTheToStringTextOfTheScreenClassifier() {
        composeTestRule.setContent {
            DeviceModesRoute(
                screenClassifier = classifier
            )
        }

        composeTestRule
            .onNodeWithTag("ScreenClassifierDescription")
            .assertTextEquals(classifier.toString())
            .assertIsDisplayed()
    }
}