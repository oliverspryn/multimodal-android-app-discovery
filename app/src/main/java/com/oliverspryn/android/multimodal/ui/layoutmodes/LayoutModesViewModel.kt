package com.oliverspryn.android.multimodal.ui.layoutmodes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update

class LayoutModesViewModel : ViewModel() {
    private val viewModelState = MutableStateFlow(LayoutModesUiState())

    val uiState = viewModelState
        .stateIn(
            viewModelScope,
            SharingStarted.Eagerly,
            LayoutModesUiState()
        )

    fun closeArticle() {
        viewModelState.update {
            it.copy(articleSelected = false)
        }
    }

    fun openArticle() {
        viewModelState.update {
            it.copy(articleSelected = true)
        }
    }
}

data class LayoutModesUiState(
    val articleSelected: Boolean = false
)
