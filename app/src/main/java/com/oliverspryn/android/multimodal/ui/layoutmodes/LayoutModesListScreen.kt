package com.oliverspryn.android.multimodal.ui.layoutmodes

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag
import androidx.compose.ui.tooling.preview.Preview
import com.oliverspryn.android.multimodal.R
import com.oliverspryn.android.multimodal.ui.theme.MultimodalTheme

@Composable
fun LayoutModesListScreen(
    onSelectArticle: () -> Unit
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .background(color = Color.LightGray)
            .fillMaxSize()
            .semantics {
                testTag = "LayoutModesListScreen"
            }
    ) {
        Button(
            onClick = { onSelectArticle() }
        ) {
            Text(text = stringResource(id = R.string.list_item))
        }
    }
}

@Preview(name = "Default Preview")
@Composable
fun PreviewLayoutModesListScreen() {
    MultimodalTheme {
        LayoutModesListScreen(
            onSelectArticle = { }
        )
    }
}
