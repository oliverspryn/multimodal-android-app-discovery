package com.oliverspryn.android.multimodal.ui.navigation

object Destinations {
    const val DeviceModes = "device-modes"
    const val Home = "home"
    const val LayoutModes = "layout-modes"
}
