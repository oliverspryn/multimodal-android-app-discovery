package com.oliverspryn.android.multimodal.ui.home

import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag

@Composable
fun HomeRoute(
    onDeviceModeButtonTap: () -> Unit,
    onLayoutModeButtonTap: () -> Unit
) {
    Box(modifier = Modifier
        .semantics {
            testTag = "HomeRoute"
        }
    ) {
        HomeScreen(
            onDeviceModeButtonTap = onDeviceModeButtonTap,
            onLayoutModeButtonTap = onLayoutModeButtonTap
        )
    }
}
