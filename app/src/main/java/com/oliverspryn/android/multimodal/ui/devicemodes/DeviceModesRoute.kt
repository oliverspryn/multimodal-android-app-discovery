package com.oliverspryn.android.multimodal.ui.devicemodes

import androidx.compose.runtime.Composable
import com.oliverspryn.android.multimodal.utils.screen.ScreenClassifier

@Composable
fun DeviceModesRoute(
    screenClassifier: ScreenClassifier
) {
    DeviceModesScreen(
        screenClassifier = screenClassifier
    )
}
