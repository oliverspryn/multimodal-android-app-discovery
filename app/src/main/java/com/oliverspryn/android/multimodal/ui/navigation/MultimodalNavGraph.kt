package com.oliverspryn.android.multimodal.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.oliverspryn.android.multimodal.ui.devicemodes.DeviceModesRoute
import com.oliverspryn.android.multimodal.ui.home.HomeRoute
import com.oliverspryn.android.multimodal.ui.layoutmodes.LayoutModesRoute
import com.oliverspryn.android.multimodal.ui.layoutmodes.LayoutModesViewModel
import com.oliverspryn.android.multimodal.utils.screen.ScreenClassifier

@Composable
fun MultimodalNavGraph(
    screenClassifier: ScreenClassifier,
    navController: NavHostController,
    modifier: Modifier
) {
    NavHost(
        navController = navController,
        startDestination = Destinations.Home,
        modifier = modifier
    ) {
        composable(Destinations.Home) {
            HomeRoute(
                onDeviceModeButtonTap = { navController.navigate(Destinations.DeviceModes) },
                onLayoutModeButtonTap = { navController.navigate(Destinations.LayoutModes) }
            )
        }

        composable(Destinations.DeviceModes) {
            DeviceModesRoute(
                screenClassifier = screenClassifier
            )
        }

        composable(Destinations.LayoutModes) {
            val viewModel: LayoutModesViewModel = viewModel()

            LayoutModesRoute(
                layoutModesViewModel = viewModel,
                screenClassifier = screenClassifier
            )
        }
    }
}
